const express = require('express')
const router = express.Router()
const orderModel = require('../model/order')
const { findById } = require('../model/order')
//const product = require('../model/product')

/////전체보기
router.get('/', (req, res) => {
    orderModel
        .find()
        .populate("rrrrrr", ["name","price"])
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        product : doc.rrrrrr,
                        quantity : doc.fffff,
                        request : {
                            type : "GET",
                            url: "http://localhost:5050/order/" + doc._id
                        }
                    }
                })
            }
            res.json(response)
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})
/////1개보기
router.get('/:orderid', (req, res) => {
    orderModel
        .findById(req.params.orderid)
        .populate("rrrrrr", ["name","price"])
        .then(doc => {
            res.json({
                message : "one order",
                orderInfo : {
                    id : doc._id,
                    product : doc.rrrrrr,
                    quantity : doc.fffff
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})

/////등록하기
router.post('/', (req, res) => {
    const order = new orderModel({
        rrrrrr : req.body.productid,
        fffff : req.body.qty
    })

    order
        .save()
        .then(doc => {
            res.json({
                message : "order product data",
                orderInfo : {
                    id : doc._id,
                    product : doc.rrrrrr,
                    qauntity : doc.fffff,
                    request : {
                        type : "GET",
                        url : "http://localhost:5050/order/" + doc._id
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})

/////order삭제하기
router.delete('/:orderid', (req, res) => {
    orderModel
        .findByIdAndDelete(req.params.orderid)
        .then(() => {
            res.json({
                message : 'order 삭제',
                request : {
                    type : "order delete",
                    url : "http://localhost:5050/order"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})





module.exports = router
