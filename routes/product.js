const express = require('express');  ////express부르고
const router = express.Router();   ///////Router()접근
const mongooseSchema = require('../model/product')


/////CRUD제작

//////불러오기get
router.get('/', (req, res) => {
    mongooseSchema
        .find()
        .then(docs => {
            res.json({
                message : "all pro",
                count : docs.length,
                productInfo : docs.map(doc => {
                    return{
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : "http://localhost:5050/product/" + doc._id
                    }
                }) 
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});
//////1개불러오기get
router.get('/:productid', (req, res) => {
    mongooseSchema
        .findById(req.params.productid)
        .then(doc => {
            res.json({
                message : "one pro",
                productInfo : {
                    id : doc._id,
                    name : doc.name,
                    price : doc.price,
                    request : "http://localhost:5050/product"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});
//////입력하기post
router.post('/', (req, res) => {
    const product = new mongooseSchema({
        name : req.body.namasix,
        price : req.body.pricesix
    })
    product
        .save()
        .then(savepro => {
            res.json({
                message : "save complete",
                productInfo : {
                    id : savepro._id,
                    name : savepro.name,
                    price: savepro.price,
                    request : "http://localhost:5050/product/" + savepro._id
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});
//////수정하기 patch
router.patch('/:productid', (req, res) => {

    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }

    mongooseSchema
        .findByIdAndUpdate(req.params.productid, { $set: updateOps})
        .then(result => {
            res.json({
                message : "update product"
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});
/////삭제하기  delete
router.delete('/:productid', (req, res) =>{
    mongooseSchema
        .findByIdAndDelete(req.params.productid)
        .then(() => {
            res.json({
                message : "one delete",
                request : "http://localhost:5050/product"
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

module.exports = router;   /////router을 모듈화시켜서 내보낸다