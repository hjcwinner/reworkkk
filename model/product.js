const mongoose = require('mongoose')
const mongooseSchema = mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    price : {
        type : String,
        required : true
    }
})

module.exports = mongoose.model('product', mongooseSchema)