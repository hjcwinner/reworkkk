const express = require('express');
const bodyParser = require('body-parser')

const app = express();
const productRoutes = require('./routes/product');
const orderRoutes = require('./routes/order')
const userRoutes = require('./routes/user');
const morgan = require('morgan');

const dotEnv = require('dotenv')
dotEnv.config()


////database
require("./config/database")




/////middleware
app.use(morgan("dev"))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))




/////router
app.use('/product', productRoutes);
app.use('/order', orderRoutes);
app.use('/user', userRoutes);


/////port
const port = process.env.PORT
app.listen(port, console.log("서버시작"));